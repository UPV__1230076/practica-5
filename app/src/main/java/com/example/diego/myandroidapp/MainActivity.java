package com.example.diego.myandroidapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends AppCompatActivity {

    Button button;
    ImageView image;
    boolean ban=true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }
    public void addListenerOnButton() {
        image = (ImageView) findViewById(R.id.imageView1);
        button = (Button) findViewById(R.id.btnChangeImage);
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                if(ban) {
                    image.setImageResource(R.drawable.android3d); //nombre del archivo imagen
                    ban = !ban;
                }
                else {
                    image.setImageResource(R.drawable.android); //nombre del archivo imagen
                    ban = !ban;
                }
            }
        });
    }

}
